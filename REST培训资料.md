# REST简介

## 概述

REST即表述性状态传递（英文：Representational State Transfer，简称REST）是Roy Fielding博士在2000年他的博士论文中提出来的一种**软件架构风格**。它是一种针对网络应用的设计和开发方式，可以降低开发的复杂性，提高系统的可伸缩性。



## REST风格产生

REST是从已有的几种基于网络的架构风格中衍生出来的 一种混合架构风格，并且添加了一些额外的约束，用来定义统一的连接器接口。

从“空”风格开始，无论是建筑还是软件，人们对架构设计的过程都有着两种常见的观点。

> - 第一种观点认为 设计师一切从零开始。一块空的石板、白板、或画板并使用熟悉的组件建造出一个架构，直到该架构满足希望的系统需求为止。

> - 第二种观点则认为设计师从作为一个整体的系统需求出发，此时没有任何约束，然后增量地识别出各种约束，并将它们应用于系统的元素之 上，以便对设计空间加以区分，并允许影响到系统行为的力量与系统协调一致， 自然地流动。

第一种观点强调创造性和无限的想象力，而第二种观点则强调限制和对系统环 境的理解。REST是使用后一种过程发展而成的。随着增量地应用一组约束，已应用的约束会将架构的过程视图区分开，下面图片以图形化的方式依次描述了这个过程。



![empty](./img/empty.png)

 “空”风格仅仅是一个空的约束集合。从架构的观点来看，空风格描述了一个 组件之间没有明显边界的系统。这就是我们描述REST的起点。



### 客户-服务器

首先被添加到我们的混合风格中的约束：**客户-服务器架构风格**。客户-服务器约束背后的原则是分离关注点。

通过分离用户接口和数据存储这两个关注点，我们改善了用户接口跨多个平台的**可移植性**；同时通过简化服务器组件，改善了系统的**可伸缩性**。然而，对于 Web来说，最重要的是这种关注点的分离允许组件独立地进化， 从而支持多个组织领域的Internet规模的需求。

![client-server](./img/client-server.png)



### 无状态

接下来再为客户-服务器交互添加一个约束：通信必须在本质上是无状态的，如客户-无状态-服务器风格那样，因此从客户到服务器的每个请求都必须包含理解该请求所必需的所有信息，不能利用任何存储在服务器上的上下文，会话状态因此要全部保存在客户端。

![no_status](./img/no_status.png)

这个约束导致了**可见性**、**可靠性**和**可伸缩性**三个架构属性。

- 改善了可见性是因为监视系统不必为了确定一个请求的全部性质而去查看该请求之外的多个请求。
- 改善了可靠性是因为 它减轻了从局部故障中恢复的任务量。
- 改善了可伸缩性是因为不必在多个请求之间保存状态，从而允许服务器组件迅速释放资源，并进一步简化其实现，因为服务器不必跨多个请求管理资源的使用。

 与大多数架构上抉择一样，无状态这一约束反映出设计上的权衡。其缺点是：由于不能将状态数据保存在服务器上的共享上下文中，因此增加了在一系列请求中发送的重复数据（每次交互的开销），可能会降低网络性能。此外，将应用状态放在客户端还降低了服务器对于一致的应用行为的控制，因为这样一来，应用就得依赖于跨多个客户端版本（例如多个浏览器窗口）的语义的正确实现。



### 缓存

为了改善网络的效率，我们添加了缓存约束，从而形成了客户-缓存无状态-服务器风格。缓存约束要求一个请求的响应中的数据被隐式地或显式地标记为可缓存的或不可缓存的。如果响应是可缓存的，那么客户端缓存就可以为以后的相同请求重用这个响应的数据。

![cache_cs](./img/cache_cs.png)

添加缓存约束的好处在于，它们有可能部分或全部消除一些交互，从而通过减少一系列交互的平均延迟时间，来提高效率、可伸缩性和用户可觉察的性能。



 ### 统一接口 

使REST架构风格区别于其他基于网络的架构风格的核心特征是，它强调组件之间要有一个统一的接口。通过在组件接口上应用通用性的软件工程原则，整体的系统架构得到了简化，交互的可见性也得到了改善。实现与它们所提供的服务是解耦的，这促进了独立的可进化性。

![union_interface](./img/union_interface.png)

为了获得统一的接口，需要有多个架构约束来指导组件的行为。REST由四个接口约束来定义：资源的识别（identification of resources）、通过表述对资源执行的操作、自描述的消息（self-descriptive messages）、以及作为应用状态引擎的超媒体。



### 分层系统

 为了进一步改善与 Internet 规模的需求相关的行为，我们添加了分层的系统约束。分层系统风格通过限制组件的行为（即，每个组件只能“看到”与其交互的紧邻层），将架构分解为若干等级的层。通过将组件对系统的知识限制在单一层内，为整个系统的复杂性设置了边界，并且提高了底层独立性。我们能够使用层来封装遗留的服务，使新的服务免受遗留客户端的影响，通过将不常用的功能转移到一个共享的中间组件中，从而简化组件的实现。中间组件还能够通过支持跨多个网络和处理器的负载均衡，来改善系统的可伸缩性。

统一-分层-客户-缓存-无状态-服务器风格：

![fenceng](./img/fenceng.png)



### 按需代码(可选) 

我们为REST添加的最后的约束为按需代码风格。通过下载并执行applet形式或脚本形式的代码，REST允许对客户端的功能进行扩展。这样， 通过减少必须被预先实现的功能的数目，简化了客户端的开发。允许在部署之后下载功能代码也改善了系统的可扩展性。

![what_need](./img/what_need.png)

### 风格推导小结 

REST由一组候选架构的架构约束组成。尽管这些约束每一个都能够独立加以考虑，但是根据它们在通用的架构风格中的来源来对它们进行描述， 使得我们理解选择它们背后的基本原理更加容易。

![jicheng](./img/jicheng.png)





## REST-统一接口详解

> 为了获得统一的接口，需要有多个架构约束来指导组件的行为。REST由四个接口约束来定义：资源的识别（identification of resources）、通过表述对资源执行的操作、自描述的消息（self-descriptive messages）、以及作为应用状态引擎的超媒体。



### 资源的识别

#### 目录结构式的 URI

REST Web 服务 URI 的直观性应该达到很容易猜测的程度。 将 URI 看作是自身配备文档说明的接口，开发人员只需很少（如果有的话）的解释或参考资料即可了解它指向什么，并获得相关的资源。为此，URI 的结构应该简单、可预测且易于理解。

> - http://example.com/customers/1234
> - http://example.com/orders/2007/10/776654
> - http://example.com/products/4554
> - http://example.com/processes/salary-increase-234



#### 名词描述

在RESTful架构中，每个网址代表一种资源（resource），所以网址中不能有动词，只能有名词，而且所用的名词往往与数据库的表格名对应。一般来说，数据库中的表都是同种记录的"集合"（collection），所以API中的名词也应该使用复数。

举例来说，有一个API提供动物园（zoo）的信息，还包括各种动物和雇员的信息，则它的路径应该设计成下面这样。

> - https://api.example.com/v1/zoos
> - https://api.example.com/v1/animals
> - https://api.example.com/v1/employees



#### 过滤信息

如果记录数量很多，服务器不可能都将它们返回给用户。API应该提供参数，过滤返回结果。

下面是一些常见的参数。

> - ?limit=10：指定返回记录的数量
> - ?offset=10：指定返回记录的开始位置。
> - ?page=2&per_page=100：指定第几页，以及每页的记录数。
> - ?sortby=name&order=asc：指定返回结果按照哪个属性排序，以及排序顺序。
> - ?animal_type_id=1：指定筛选条件



在考虑基于 REST 的 Web 服务的 URI 结构时，需要指出的一些附加指导原则包括：

- 隐藏服务器端脚本技术文件扩展名（.jsp、.php、.asp）——如果有的话，以便您能够移植到其他脚本技术而不用更改 URI。
- 将所有内容保持小写。
- 将空格替换为连字符或下划线（其中一种或另一种）。
- 尽可能多地避免查询字符串。
- 如果请求 URI 用于部分路径，与使用 404 Not Found 代码不同，应该始终提供缺省页面或资源作为响应。

URI 还应该是静态的，以便在资源发生更改或服务的实现发生更改时，链接保持不变。 这可以实现书签功能。 URI 中编码的资源之间的关系与在存储资源的位置表示资源关系的方式无关也是非常重要的。



### 资源执行的操作

#### 显式地使用 HTTP 方法

对于资源的具体操作类型，由HTTP 方法表示。常用的HTTP方法有下面五个（括号里是对应的SQL命令）。

> - GET（SELECT）：从服务器取出资源（一项或多项）。
> - POST（CREATE）：在服务器新建一个资源。
> - PUT（UPDATE）：在服务器更新资源（客户端提供改变后的完整资源）。
> - PATCH（UPDATE）：在服务器更新资源（客户端提供改变的属性）。
> - DELETE（DELETE）：从服务器删除资源。

还有两个不常用的HTTP动词。

> - HEAD：获取资源的元数据。
> - OPTIONS：获取信息，关于资源的哪些属性是客户端可以改变的。

下面是一些例子。

> - GET /zoos：列出所有动物园
> - POST /zoos：新建一个动物园
> - GET /zoos/ID：获取某个指定动物园的信息
> - PUT /zoos/ID：更新某个指定动物园的信息（提供该动物园的全部信息）
> - PATCH /zoos/ID：更新某个指定动物园的信息（提供该动物园的部分信息）
> - DELETE /zoos/ID：删除某个动物园
> - GET /zoos/ID/animals：列出某个指定动物园的所有动物
> - DELETE /zoos/ID/animals/ID：删除某个指定动物园的指定动物



#### HTTP方法映射

服务程序中的特定操作被映射成为标准的HTTP方法，如标识一个顾客的URI上的GET方法正好相当于`getCustomerList`操作

![uri_interface](./img/uri_interface.png)



为什么使用标准方法如此重要？从根本上说，它使你的应用成为Internet的一部分。应用程序为Internet所做的贡献，与它添加到Web中的资源数量成比例。采用RESTful方式，一个应用可能会向Web中添加数以百万计的客户URI；如果采用CORBA技术并维持应用的原有设计方式，那它的贡献大抵只是一个“端点（endpoint）”——就好比一个非常小的门，仅仅允许有钥匙的人进入其中的资源域。



#### 自描述的消息

在使用HTTP的RESTful应用中，消息利用两种东西实现了自描述，

- 其一，通过使用无状态的统一接口；
- 其二，通过使用HTTP报头（Header），它描述了消息内容，除此之外还包括HTTP实现相关的各协议方面（如内容协商、针对缓冲的条件请求和优化并发等等）。

通过检查使用的HTTP方法和请求/响应报头，像代理或缓存这样的中间实体就能够破译哪部分协议（即HTTP）正在被使用以及它们是如何被使用的。这类自描述信息保证了客户端和服务器之间的交互是可见的（如，对缓存的使用），可靠的（如检测局部故障并从中恢复）和可伸缩的。每条信息都包含足够的信息来描述如何处理消息本身。例如，一个网络媒体类型本身就定义了能运行该媒体的解析器（以前被称为MIME类型）。



#### 作为应用状态引擎的超媒体

RESTful API最好做到超媒体，即返回结果中提供链接，连向其他 API 方法，使得用户不查文档，也知道下一步应该做什么。

比如，当用户向`api.example.com`的根目录发出请求，会得到这样一个文档。

> ```
> {"link": {
>   "rel":   "collection https://www.example.com/zoos",
>   "href":  "https://api.example.com/zoos",
>   "title": "List of zoos",
>   "type":  "application/vnd.yourformat+json"
> }}
>
> ```

上面代码表示，文档中有一个link属性，用户读取这个属性就知道下一步该调用什么API了。rel表示这个API与当前网址的关系（collection关系，并给出该collection的网址），href表示API的路径，title表示API的标题，type表示返回类型。

Hypermedia API的设计被称为[HATEOAS](http://en.wikipedia.org/wiki/HATEOAS)。Github的API就是这种设计，访问[api.github.com](https://api.github.com/)会得到一个所有可用API的网址列表。

> ```
> {
>   "current_user_url": "https://api.github.com/user",
>   "authorizations_url": "https://api.github.com/authorizations",
>   // ...
> }
>
> ```

从上面可以看到，如果想获取当前用户的信息，应该去访问[api.github.com/user](https://api.github.com/user)，然后就得到了下面结果。

> ```
> {
>   "message": "Requires authentication",
>   "documentation_url": "https://developer.github.com/v3"
> }
>
> ```

上面代码表示，服务器给出了提示信息，以及文档的网址。



## 练习

对一篇文章进行增删改查接口应该如何设计？

示例：https://developer.github.com/v3/gists/comments/



## 参考资料

[基于 REST 的 Web 服务：基础](https://www.ibm.com/developerworks/cn/webservices/ws-restful/index.html)

http://www.ruanyifeng.com/blog/2014/05/restful_api.html

http://www.infoq.com/cn/articles/subbu-allamaraju-rest

http://www.cnblogs.com/RicCC/archive/2008/12/28/REST-API-must-be-hypertext-driven.html

http://www.infoq.com/cn/news/2008/12/restapi-must-be-hypertext-driven

https://www.ibm.com/developerworks/webservices/library/ws-restful/?S_TACT=105AGX52&S_CMP=content

Roy Fielding's dissertation - [Architectural Styles and the Design of Network-based Software Architectures](http://www.ics.uci.edu/~fielding/pubs/dissertation/top.htm).
Roy Fielding's "[REST APIs must be hypertext-driven](http://roy.gbiv.com/untangled/2008/rest-apis-must-be-hypertext-driven)" blog post.
Leonard Richardson & Mike Amundsen's [RESTful Web APIs](http://restfulwebapis.org/)





